# fingerprinter
This library contains the algorithms needed to generate various fingerprints, as well as functions to retrieve the needed packets to generate such fingerprints. Currently supported fingerprints are listed below.

## JA3 & JA3S
Derived from https://github.com/salesforce/ja3.

JA3 fingerprints can be generated for devices that attempt to connect to a server, while JA3S fingerprints can be generated for servers. JA3 fingerprints can only be generated if the TLS "Client Hello" packet can be intercepted. If you need a JA3 fingerprint, then you will need to supply the packet.



## HAASH (client and server versions)
Derived from https://github.com/salesforce/hassh.