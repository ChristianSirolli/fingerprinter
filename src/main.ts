import ipaddr from 'ipaddr.js';
import type { IPv4, IPv6 } from 'ipaddr.js';
import md5 from 'md5';

class Fingerprinter {
    constructor(target: IPv4 | IPv6) {
        this.target = target;
    }
    static TLS_GREASE_TABLE = {
        0x0a0a: true, 0x1a1a: true, 0x2a2a: true, 0x3a3a: true,
        0x4a4a: true, 0x5a5a: true, 0x6a6a: true, 0x7a7a: true,
        0x8a8a: true, 0x9a9a: true, 0xaaaa: true, 0xbaba: true,
        0xcaca: true, 0xdada: true, 0xeaea: true, 0xfafa: true
    }
    static TLS_HANDSHAKE = 22;
    static #tls_convert_to_ja3_segment() {}
    static #tls_process_extensions() {}
    static genJA3Fingerprint(TLSVersion: number, Ciphers: Array<number>, Extensions?: Array<number> = [], EllipticCurves?: Array<number> = [], EllipticCurvePointFormats?: number | undefined = []): Record<string, any> {
        let ja3 = [TLSVersion, Ciphers.join('-'), Extensions.join('-'), EllipticCurves.join('-'), EllipticCurvePointFormats == undefined ? '' : EllipticCurvePointFormats].join(',');
        let record = {
            ja3,
            ja3_digest: string = md5(ja3)
        };
        return record;
    }
    static genJA3SFingerprint(TLSVersion: number, Cipher: Array<number>, Extensions?: Array<number> = []): Record<string, any> {
        let ja3s = [TLSVersion, Cipher.join('-'), Extensions.join('-')].join(',');
        let record = {
            ja3s,
            ja3s_digest: string = md5(ja3s)
        };
        return record;
    }
    static genClientHAASHFingerprint(): Record<string, any> { }
    static genServerHAASHFingerprint(): Record<string, any> { }
    getServerHAASH(port?: number = 22): Record<string, any> {
        // connect to SSH
        Fingerprinter.genServerHAASHFingerprint();
    }
    getJA3S(ssl_port?: number = 443): Record<string, any> {
        // connect with TLS
        Fingerprinter.genJA3SFingerprint();
    }
}